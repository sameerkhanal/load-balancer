package com.loadbalancer.api.services;

import java.util.concurrent.ExecutionException;

public interface PostReqestService {

    String register() throws InterruptedException, ExecutionException;

    String changePassword() throws InterruptedException, ExecutionException;

}
