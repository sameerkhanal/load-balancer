package com.loadbalancer.api.services.impl;

import com.loadbalancer.api.config.RibbonConfiguration;
import com.loadbalancer.api.services.GetRequestService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RibbonClient(name = "load-balancer", configuration = RibbonConfiguration.class)
public class GetRequestServiceImpl implements GetRequestService {


    @Qualifier("loadBalanced")
    private final RestTemplate restTemplate;

    public GetRequestServiceImpl(@Qualifier("loadBalanced") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public String login() {
        return this.restTemplate.getForObject(
                "http://load-balancer/login", String.class);
    }
}
