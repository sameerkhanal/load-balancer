package com.loadbalancer.api.services.impl;

import com.loadbalancer.api.services.PostReqestService;
import org.springframework.core.env.Environment;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static java.lang.Thread.sleep;

@Service
public class PostRequestServieImpl implements PostReqestService {

    private final Environment environment;

    private final RestTemplate restTemplate;

    public PostRequestServieImpl(Environment environment, RestTemplate restTemplate) {
        this.environment = environment;
        this.restTemplate = restTemplate;
    }

    @Override
    @Async
    @Retryable(value = {InterruptedException.class, ExecutionException.class}, maxAttempts = 20, backoff = @Backoff(delay = 500, multiplier = 3))
    public String register() throws ExecutionException, InterruptedException {
        return getResponse(registerToAllServers());
    }

    @Override
    @Retryable(value = {InterruptedException.class, ExecutionException.class}, maxAttempts = 20, backoff = @Backoff(delay = 500, multiplier = 3))
    public String changePassword() throws InterruptedException, ExecutionException {
        return getResponse(changePasswordToAllServers());
    }

    private List<Future<String>> changePasswordToAllServers() {

        List<Future<String>> futureList = new ArrayList<>();
        getServerList().forEach(server -> {
            futureList.add(new AsyncResult<>(restTemplate.postForObject("http://" + server + "/register", null, String.class)));
        });
        return futureList;
    }

    private List<Future<String>> registerToAllServers() {
        List<Future<String>> futureList = new ArrayList<>();
        getServerList().forEach(server -> {
            futureList.add(new AsyncResult<>(restTemplate.postForObject("http://" + server + "/changePassword", null, String.class)));
        });
        return futureList;
    }

    private String getResponse(List<Future<String>> registerServers) throws ExecutionException, InterruptedException {
        while (true) {
            for (Future<String> stringFuture : registerServers) {
                if (stringFuture.isDone())
                    return stringFuture.get();
                sleep(1000);
            }
        }
    }

    private List<String> getServerList() {
        return Arrays.asList((environment.getProperty("spring.application.name") + ".ribbon.listOfServers").split(","));
    }


}
