package com.loadbalancer.api.controller;

import com.loadbalancer.api.services.GetRequestService;
import com.loadbalancer.api.services.PostReqestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@CrossOrigin
public class AuthenticationResource {

    private final GetRequestService getRequestService;

    private final PostReqestService postReqestService;

    public AuthenticationResource(GetRequestService getRequestService, PostReqestService postReqestService) {
        this.getRequestService = getRequestService;
        this.postReqestService = postReqestService;
    }

    @GetMapping("/login")
    private String login() {
        return getRequestService.login();
    }

    @PostMapping("/changePassword")
    @ResponseStatus(HttpStatus.CREATED)
    private String changePassword() throws ExecutionException, InterruptedException {
        return postReqestService.changePassword();
    }

    @PostMapping("register")
    @ResponseStatus(HttpStatus.CREATED)
    private String register() throws ExecutionException, InterruptedException {
        return postReqestService.register();
    }


}
