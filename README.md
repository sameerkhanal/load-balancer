**Client Side Load Balancing Using Netflix Ribbon and Spring Boot**

Three has been exposed that takes request from the client and forwards the request to the service instance

GET:

   ` /login`

***Netflix Ribbon*** is used to load balance this GET request.It uses round robin rule for selecting the server by pinging the client on fixed interval
in this case 15000 milisecond, which can be configured on application.yml, to check server availability.

POST:

  ```
  /register 
  
  /changePassword
```


Post request are forwarded to server all server list asyncronously ,using Future, and the first successful response is returned to the client.
To ensure the update in all instace ***Spring-retry*** is used, which retry te request to max-attemt or until successful response with backof on 500 milisecond 
with multiplier of 3 in every 500 milisecond.

Basic Matric indication is used using ***Spring Boot Actuator***


Go to http://localhost:8080/actuator for health matics and info details.


To run locally:
`./gradlew bootRun`

In your browser, go to http://localhost:8080/ with port configurable in application.yml

Or create a runnable jar file:

`./gradlew bootJar`
